\select@language {vietnam}
\beamer@sectionintoc {1}{T\IeC {\h \ocircumflex }ng quan v\IeC {\`\ecircumflex } Oracle Database Administration}{3}{0}{1}
\beamer@sectionintoc {2}{C\IeC {\'a}c b\IeC {\uhorn }\IeC {\'\ohorn }c c\IeC {\`a}i \IeC {\dj }\IeC {\d \abreve }t Oracle Database Managerment System}{6}{0}{2}
\beamer@sectionintoc {3}{T\IeC {\d a}o m\IeC {\d \ocircumflex }t database m\IeC {\'\ohorn }i trong Oracle}{31}{0}{3}
\beamer@sectionintoc {4}{S\IeC {\h \uhorn }a \IeC {\dj }\IeC {\h \ocircumflex }i m\IeC {\d \ocircumflex }t Database}{39}{0}{4}
\beamer@sectionintoc {5}{C\IeC {\'\acircumflex }p ph\IeC {\'e}p truy c\IeC {\d \acircumflex }p v\IeC {\`a}o Database}{47}{0}{5}
\beamer@sectionintoc {6}{\IeC {\DJ }\IeC {\u a}ng k\IeC {\'\i } ng\IeC {\uhorn }\IeC {\`\ohorn }i d\IeC {\`u}ng v\IeC {\`a} b\IeC {\h a}o m\IeC {\d \acircumflex }t h\IeC {\d \ecircumflex } th\IeC {\'\ocircumflex }ng}{54}{0}{6}
\beamer@sectionintoc {7}{Ki\IeC {\h \ecircumflex }m so\IeC {\'a}t v\IeC {\`a} theo d\IeC {\~o}i ng\IeC {\uhorn }\IeC {\`\ohorn }i d\IeC {\`u}ng}{64}{0}{7}
\beamer@sectionintoc {8}{Gi\IeC {\'a}m s\IeC {\'a}t v\IeC {\`a} t\IeC {\'\ocircumflex }i \IeC {\uhorn }u h\IeC {\'o}a CSDL}{72}{0}{8}
\beamer@sectionintoc {9}{Sao l\IeC {\uhorn }u v\IeC {\`a} kh\IeC {\^o}i ph\IeC {\d u}c CSDL}{92}{0}{9}
\beamer@sectionintoc {10}{Demo}{96}{0}{10}
